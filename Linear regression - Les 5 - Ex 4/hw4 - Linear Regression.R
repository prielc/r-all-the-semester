titanic <- read.csv("train.csv")

str(titanic)

#B - Are there attributes with missing values? If there are fill the missing values with the mean value of that attribut

#check if there is NA vlaue
any(is.na(titanic))

#A loop for replace the NA value with the mean value of that attribute 
for(i in 1:ncol(titanic)){
  titanic[is.na(titanic[,i]), i] <- mean(titanic[,i], na.rm = TRUE)
}

#C - Turn Survival into a factor with the right values

titanic$Survived <- factor(titanic$Survived , levels = c(0,1), labels = c('No', 'Yes'))

#check if it's work
levels(titanic$Survived)

#install ggplot2
install.packages('ggplot2')
library(ggplot2)

#D - Use ggplot histogram to investigate how Age, Fare and SibSp affect Survival (submit the charts, use print screen)

#how Age affect Survival
pl1 <- ggplot(titanic, aes(x = Age, fill=Survived))
pl1 <- pl1 + geom_histogram()

#how Fare affect Survival
pl2 <- ggplot(titanic, aes(x = Fare, fill=Survived))
pl2 <- pl2 + geom_histogram()

#how SibSp affect Survival
pl3 <- ggplot(titanic, aes(x = SibSp, fill=Survived))
pl3 <- pl3 + geom_histogram()

#E - Select relevant features for machine learning
#Yalla Beitar!!