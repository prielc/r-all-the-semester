iris

library(ggplot2)

pl <- ggplot(iris, aes(Petal.Length, Petal.Width, color = Species))
pl <- pl + geom_point(size = 4)

pl1 <- ggplot(iris, aes(Petal.Length, Sepal.Length, color = Species))
pl1 <- pl1 + geom_point(size = 4)

?kmeans

set.seed(101)

irisCluster <- kmeans(iris[,1:4], 3, nstart = 20)

irisCluster$cluster

table(irisCluster$cluster, iris$Species)

install.packages('cluster')
library('cluster')

clusplot(iris, irisCluster$cluster, color = T, shade = T, labels = 0, lines = 0)

#----------SQL----------

install.packages('RSQLite')
library(RSQLite)
install.packages('sqldf')
library(sqldf)

str(iris)

iris$Petal_Length <- iris$Petal.Length

Setosa_large <- sqldf('
                      SELECT *
                      FROM iris i1
                      WHERE i1.Species = "setosa"
                      AND i1.Petal_Length > 1.3
                      ')
